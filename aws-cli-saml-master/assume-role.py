import boto3
import configparser
import os
import sys
from os.path import expanduser
import getopt

class assumerole:
    def __init__(self, execution_profile, profile, account, role):
        self.profile = profile
        self.account = account
        self.role = role
        self.execution_profile = execution_profile

        self.region = "us-east-1"
        self.outputformat = "json"
        self.awsconfigfile = "/.aws/credentials"

        self.token = self.__assume_role()
        self.__setCredentialFile()

    def __assume_role(self):
        boto3.setup_default_session(profile_name=self.execution_profile)
        sts_client = boto3.client('sts', verify=False)
        return sts_client.assume_role(RoleArn="arn:aws:iam::{}:role/{}".format(self.account,self.role), RoleSessionName="{}-{}".format(self.account,self.role))

    def __setCredentialFile(self):
        # Toss the STS credentials into the aws config file
        home = expanduser("~")
        filename = home + self.awsconfigfile

        # make .aws dir if it does not exist
        try:
            os.mkdir("{}/.aws".format(home))
        except FileExistsError:
            print("Credential folder already exists; skipping")

        # Create credentials file if it doesn't exist
        open(filename, 'a').close()

        # read credential file
        config = configparser.RawConfigParser()
        config.read(filename)

        # create the config section if it doesn't already exist
        if not config.has_section(self.profile):
            config.add_section(self.profile)

        config.set(self.profile, 'output', self.outputformat)
        config.set(self.profile, 'region', self.region)
        config.set(self.profile, 'aws_access_key_id', self.token['Credentials']['AccessKeyId'])
        config.set(self.profile, 'aws_secret_access_key', self.token['Credentials']['SecretAccessKey'])
        config.set(self.profile, 'aws_session_token', self.token['Credentials']['SessionToken'])

        # # Write the updated config file
        with open(filename, 'w+') as configfile:
            config.write(configfile)

        print('\n\n----------------------------------------------------------------')
        print('Your new access key pair has been stored in the AWS configuration file {} under the {} profile.'.format(filename,self.profile))
        print('Note that it will expire at {}.'.format(self.token['Credentials']['Expiration']))
        print('After this time, you may safely rerun this script to refresh your access key pair.')
        print('To use this credential, call the AWS CLI with the --profile option (e.g. aws --profile {} ec2 describe-instances).'.format(self.profile))
        print('----------------------------------------------------------------\n\n')     

#############################################################################  
def main(argv):
    ## defaults
    execution_profile = "saml"
    assume_role_profile = "assumerole"
    account = None
    role = None

    try:
        opts, args = getopt.getopt(argv,"h:",["execution-profile=","assume-role-profile=","account=","role="])
    except getopt.GetoptError:
        print('assume-role.py --account <account #> --role <role name> --execution-profile <OPTIONAL profile> --assume-role-profile <OPTIONAL profile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('assume-role.py --account <account #> --role <role name> --execution-profile <OPTIONAL profile> --assume-role-profile <OPTIONAL profile>')
            sys.exit()
        elif opt in ("--execution-profile"):
            if arg == '' or arg == None:
                print("Profile can not be blank")
                sys.exit(2)
            execution_profile = str(arg)
        elif opt in ("--assume-role-profile"):
            if arg == '' or arg == None:
                print("Profile can not be blank")
                sys.exit(2)
            assume_role_profile = str(arg)
        elif opt in ("--account"):
            if arg == '' or arg == None:
                print("Account number can not be blank")
                sys.exit(2)
            account = str(arg)
        elif opt in ("--role"):
            if arg == '' or arg == None:
                print("Role can not be blank")
                sys.exit(2)
            role = str(arg)

    if account == None or role == None:
        print("Missing account number or role name")
        print('assume-role.py --account <account #> --role <role name> --execution-profile <OPTIONAL profile> --assume-role-profile <OPTIONAL profile>')
        sys.exit(2)

    assumerole(execution_profile, assume_role_profile, account, role)

#####
main(sys.argv[1:])
