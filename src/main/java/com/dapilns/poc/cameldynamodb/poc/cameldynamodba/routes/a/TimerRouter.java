package com.dapilns.poc.cameldynamodb.poc.cameldynamodba.routes.a;

import java.time.LocalDateTime;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TimerRouter extends RouteBuilder{

	@Autowired
	private GetCurrentTime getCurrentTime;
	
	@Override
	public void configure() throws Exception {
		//may be listen to queue
		//transform 
		//database
		from("timer:first-timer").
			bean(getCurrentTime, "getCurrentTime").
		to("log:first-timer");
	}

}

@Component
class GetCurrentTime{
	public String getCurrentTime (){
		return ("Time now is " + LocalDateTime.now());
	}
	
	
}